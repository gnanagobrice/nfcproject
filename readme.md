# Etude de cas
## Proposition d'une application fonctionnant avec le système NFC
Système d'authenfication et pointage des employées d'une entreprise
## Description des fonctionnalité de l'application
Dans cette partie, nous décrivons les fonctionnalité de l'application dans sa généralité

### Connexion Pear To Pear entre deux appareils (celui de l'employé et celui de l'entreprise)
NB: Ces appreils devrait intégrer dans leur architecture la technologie NFC
### Authentification du client via sont téléphone 
    Une authentification est effectué quand les deux appareils sont connectés

### Pointage de l'application
Il s'agit de marquer présent ou absent les employés de l'entreprise

## Proposition de l'architecture globale de plateforme imaginée
